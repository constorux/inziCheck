import 'package:flutter/material.dart';

abstract class ReBitWidget<T extends ReBitState> extends StatefulWidget {
  final T Function(dynamic) _setState;
  ReBitWidget(this._setState);

  @override
  T createState() => _setState(this);

  Widget onBuild(BuildContext context, T state);
}

enum ReBitTypes { neutral, loading, error }

class ReBitState<W extends ReBitWidget<dynamic>, T> extends State<W> {
  final W widget;
  T type;
  ReBitState(this.widget, this.type);

  @override
  void initState() {
    super.initState();
    this.init();
  }

  void init() {}

  /// overriding the default setState method to prevent the widget from
  /// trying to rebuild when it's not mounted
  @override
  void setState(void Function() fn) {
    (mounted) ? super.setState(fn) : fn();
  }

  @override
  Widget build(BuildContext context) {
    return widget.onBuild(context, this);
  }
}
