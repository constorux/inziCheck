import 'package:flutter/material.dart';

class Consts {
  // ignore: non_constant_identifier_names
  static final Color APPBAR_LIGHT_COLOR = Color(0xff404040);
  // ignore: non_constant_identifier_names
  static final String URL_REPO = "https://gitlab.com/constorux/inziCheck";
}
