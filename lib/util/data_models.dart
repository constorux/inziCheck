class Landkreis {
  final String name;
  final int bundeslandID;
  final double inzidenz7Day;
  final int? casesAbsolute;
  final int? tode;
  final Bundesland? bundesland;
  Landkreis(
      {required this.name,
      required this.bundeslandID,
      required this.inzidenz7Day,
      required this.casesAbsolute,
      required this.tode,
      this.bundesland});

  Landkreis copyWith({required Bundesland bundesland}) {
    return Landkreis(
        name: this.name,
        bundeslandID: this.bundeslandID,
        inzidenz7Day: this.inzidenz7Day,
        casesAbsolute: this.casesAbsolute,
        tode: this.tode,
        bundesland: bundesland);
  }
}

class Bundesland {
  final int id;
  final String name;
  final String abkuerzung;
  final double inzidenz;
  final double hospitalisierungsrate;

  Bundesland(
      {required this.id,
      required this.name,
      required this.abkuerzung,
      required this.inzidenz,
      required this.hospitalisierungsrate});
}
