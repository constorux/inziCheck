import 'package:flutter/material.dart';

class SharedWidgets {
  static Widget section(
      {required BuildContext context,
      required String title,
      required List<Widget> children,
      double bottomPadding = 60,
      double maxChildWidth = double.infinity,
      Alignment childAlignment = Alignment.topCenter,
      Widget? action,
      EdgeInsets? titlePadding = const EdgeInsets.only(bottom: 20)}) {
    return Container(
      padding: EdgeInsets.only(bottom: bottomPadding),
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Container(
          padding: titlePadding,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    child: Text(title,
                        style: Theme.of(context).textTheme.headline3)),
                if (action != null) action
              ]),
        ),
        Container(
            alignment: childAlignment,
            child: Container(
              constraints: BoxConstraints(maxWidth: maxChildWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  for (Widget child in children) child,
                ],
              ),
            ))
      ]),
    );
  }

  static Widget coinImage({required Widget? img}) {
    return img == null
        ? Container()
        : Padding(
            padding: const EdgeInsets.only(right: 12),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: img,
            ),
          );
  }

  static Widget labeledTextField({
    required String label,
    double fontSize = 12,
    double vertialPadding = 10,
    Widget? prefix,
    String? prefixText,
    String initialValue = "",
    Widget? suffix,
    String? suffixText,
  }) {
    final textFieldStyle = TextStyle(
        fontWeight: FontWeight.bold, fontSize: fontSize, color: Colors.white);

    //if (prefixText != null) prefix = Text(prefixText, style: textFieldStyle);
    //if (suffixText != null) suffix = Text(suffixText, style: textFieldStyle);

    return Container(
        padding: EdgeInsets.symmetric(vertical: vertialPadding),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              label, //.toUpperCase(),
              style: textFieldStyle.copyWith(
                  /*fontWeight: FontWeight.normal,*/ color: Colors.grey[400]),
            ),
            SizedBox(
              width: 200,
              child: TextFormField(
                style: textFieldStyle,
                textAlign: TextAlign.end,
                decoration: InputDecoration(
                    prefix: prefix,
                    suffix: suffix,
                    suffixText: suffixText,
                    border: OutlineInputBorder()),
              ),
            )
          ],
        ));
  }

  static Widget loadingWidget(BuildContext context,
      {required bool isError,
      Function? onRetryClick,
      double minHeight = 0.0,
      Widget? errorChild,
      Widget? loadingChild,
      String? errorText}) {
    return Container(
        constraints: BoxConstraints(minHeight: minHeight),
        child: isError
            ? progressErrorIndicator(
                context: context,
                onRetryClick: onRetryClick,
                child: errorChild,
                errorText: errorText)
            : loadingChild ?? progressIndicator());
  }

  static Widget progressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  static Widget progressErrorIndicator(
      {required BuildContext context,
      Function? onRetryClick,
      Widget? child,
      String? errorText}) {
    return Center(
      child: GestureDetector(
        onTap: () => {if (onRetryClick != null) onRetryClick()},
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.all(10),
          child: child ??
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (onRetryClick != null)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Icon(Icons.refresh),
                    ),
                  Text(
                    errorText ?? "Daten konnten nicht geladen werden",
                    textAlign: TextAlign.center,
                  )
                ],
              ),
        ),
      ),
    );
  }
}
