part of 'landkreis_daten_cubit.dart';

@freezed
class LandkreisDatenState with _$LandkreisDatenState {
  const factory LandkreisDatenState.loading() = _Loading;
  const factory LandkreisDatenState.error() = _Error;
  const factory LandkreisDatenState.completed(List<Landkreis> counties) =
      _Completed;
}
