import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:corona_daten/services/rki_api.dart';
import 'package:corona_daten/util/data_models.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'landkreis_daten_state.dart';
part 'landkreis_daten_cubit.freezed.dart';

class LandkreisDatenCubit extends Cubit<LandkreisDatenState> {
  LandkreisDatenCubit() : super(LandkreisDatenState.loading()) {
    init();
  }

  Future<List<String>> loadFromFS() async {
    return (await SharedPreferences.getInstance())
            .getStringList("landkreise") ??
        [];
  }

  Future<void> saveToFS(List<Landkreis> landkreise) async {
    (await SharedPreferences.getInstance())
        .setStringList("landkreise", landkreise.map((e) => e.name).toList());
  }

  Future<Landkreis> _getLandkreis({required String name}) async {
    return RKIApi.getLandkreis(name: name);
  }

  Future<List<Landkreis>> getData(List<String> names) async {
    List<Landkreis> result = [];
    for (String s in names) {
      await _getLandkreis(name: s).then((value) {
        result.add(value);
      }, onError: (_) {
        throw Exception("Error loading Landkreis $s");
      });
    }
    return result;
  }

  //refresh() => init();
  init() {
    emit(LandkreisDatenState.loading());
    loadFromFS().then(
        (names) => getData(names).then(
            (value) => emit(LandkreisDatenState.completed(value)),
            onError: (_) => emit(LandkreisDatenState.error())),
        onError: (_) => emit(LandkreisDatenState.error()));
  }

  removeItem(List<Landkreis> landkreise, String landkreis) async {
    emit(LandkreisDatenState.loading());
    landkreise.removeWhere((element) => element.name == landkreis);
    await saveToFS(landkreise);
    emit(LandkreisDatenState.completed(landkreise));
  }

  addItem(List<Landkreis> landkreise, String landkreis) async {
    if (landkreise.indexWhere((element) => element.name == landkreis) >= 0)
      return;

    emit(LandkreisDatenState.loading());
    await _getLandkreis(name: landkreis).then((value) async {
      landkreise.add(value);
      await saveToFS(landkreise);
      emit(LandkreisDatenState.completed(landkreise));
    }, onError: (_) {
      emit(LandkreisDatenState.error());
    });
  }

  static Color getInzidenzColor(List<Landkreis> kreise, double inzidenz) {
    double maximum = kreise.fold(0, (p, e) => max(p, e.inzidenz7Day));

    double value = inzidenz / maximum;

    if (value < 0.2)
      return Colors.yellow[600]!;
    else if (value < 0.4)
      return Colors.orange[500]!;
    else if (value < 0.6)
      return Colors.orange[900]!;
    else if (value < 0.8)
      return Colors.red[500]!;
    else if (value < 0.9)
      return Colors.red[700]!;
    else
      return Colors.pink[800]!;
  }
}
