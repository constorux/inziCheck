import 'package:bloc/bloc.dart';
import 'package:corona_daten/services/rki_api.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'landkreis_list_state.dart';
part 'landkreis_list_cubit.freezed.dart';

class LandkreisListCubit extends Cubit<LandkreisListState> {
  LandkreisListCubit() : super(LandkreisListState.loading()) {
    getLandkreisNames();
  }

  void getLandkreisNames() {
    emit(LandkreisListState.loading());
    RKIApi.getLandkreisList().then(
        (value) => emit(LandkreisListState.neutral(value)),
        onError: (e) => emit(LandkreisListState.error("Ladefehler")));
  }
}
