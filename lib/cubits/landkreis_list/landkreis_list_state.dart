part of 'landkreis_list_cubit.dart';

@freezed
class LandkreisListState with _$LandkreisListState {
  const factory LandkreisListState.loading() = _Loading;
  const factory LandkreisListState.error(String message) = _Error;
  const factory LandkreisListState.neutral(List<String> counties) = _Neutral;
}
