import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'settings_state.dart';
part 'settings_cubit.freezed.dart';

class SettingsThemeMode {
  final int key;
  final ThemeMode mode;
  final String name;

  const SettingsThemeMode(this.key, this.mode, this.name);
  const SettingsThemeMode.system()
      : this(0, ThemeMode.system, "an Gerät angepasst");
}

class SettingsCubit extends Cubit<SettingsState> {
  static const themeModes = [
    SettingsThemeMode.system(),
    SettingsThemeMode(1, ThemeMode.light, "Hell"),
    SettingsThemeMode(2, ThemeMode.dark, "Dunkel"),
  ];

  static String modeToString(ThemeMode mode) {
    return themeModes
        .firstWhere((e) => e.mode == mode,
            orElse: () => SettingsThemeMode.system())
        .name;
  }

  static SettingsThemeMode? modeFromMode(ThemeMode mode) {
    for (SettingsThemeMode item in themeModes) {
      if (item.mode == mode) return item;
    }
    return null;
  }

  SettingsCubit() : super(SettingsState.neutral(ThemeMode.system)) {
    init();
  }

  init() async {
    int modeInt =
        (await SharedPreferences.getInstance()).getInt("setting_dark") ?? 0;

    ThemeMode mode = themeModes
        .firstWhere((e) => e.key == modeInt,
            orElse: () => SettingsThemeMode.system())
        .mode;

    emit(SettingsState.neutral(mode));
  }

  setThemeMode(int value) async {
    //emit(SettingsState.loading());
    (await SharedPreferences.getInstance()).setInt("setting_dark", value);
    init();
  }
}
