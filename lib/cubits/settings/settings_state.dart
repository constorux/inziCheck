part of 'settings_cubit.dart';

@freezed
class SettingsState with _$SettingsState {
  //const factory SettingsState.loading() = _Loading;
  const factory SettingsState.neutral(ThemeMode mode) = _Neutral;
}
