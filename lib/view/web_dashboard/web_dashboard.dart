import 'package:corona_daten/services/rki_api.dart';
import 'package:corona_daten/util/consts.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebDashboard extends StatefulWidget {
  const WebDashboard({Key? key}) : super(key: key);

  @override
  State<WebDashboard> createState() => _WebDashboardState();
}

class _WebDashboardState extends State<WebDashboard> {
  int loadingState = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop()),
        title: Text(
          'RKI Dashboard',
          style: TextStyle(
              color: Theme.of(context).brightness == Brightness.light
                  ? Consts.APPBAR_LIGHT_COLOR
                  : Color(0xffFFFFFF)),
        ),
      ),
      body: Column(
        children: [
          if (loadingState < 100)
            LinearProgressIndicator(
              color: Colors.orange,
              value: loadingState.toDouble() / 100,
            ),
          Expanded(
            child: WebView(
              initialUrl: URL_RKI_DASHBOARD,
              javascriptMode: JavascriptMode.unrestricted,
              onProgress: (v) => setState(() => loadingState = v),
            ),
          ),
        ],
      ),
    );
  }
}
