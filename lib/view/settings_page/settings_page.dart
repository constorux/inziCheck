import 'package:corona_daten/cubits/settings/settings_cubit.dart';
import 'package:corona_daten/util/consts.dart';
import 'package:corona_daten/view/oss_license_page/oss_licenses_page.dart';
import 'package:easy_dialogs/single_choice_confirmation_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  void showBugReportDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('👋 Hi, melde einen Fehler'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text(
                    'Du hast einen Fehler in der App gefunden? Dann schreib mir gerne eine Email an:\n\n constorux@gmail.com'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void showDataSourceDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Datenquellen'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text(
                    'Die App verwendet folgende APIs:\n\n- API des RKI über arcgis \n- corona-zahlen.org'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  SettingsTile _themeTile(BuildContext context, ThemeMode mode) {
    return SettingsTile(
        title: 'App Helligkeit',
        subtitle: SettingsCubit.modeToString(mode),
        leading: Icon(Icons.palette),
        onPressed: (_) => showDialog(
            context: context,
            builder: (context) =>
                SingleChoiceConfirmationDialog<SettingsThemeMode>(
                  title: Text("App Helligkeit"),
                  initialValue: SettingsCubit.modeFromMode(mode),
                  items: SettingsCubit.themeModes,
                  itemBuilder: (item) => Text(item.name),
                  onSubmitted: (value) =>
                      context.read<SettingsCubit>().setThemeMode(value.key),
                )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop()),
        title: Text(
          "Einstellungen",
          style: TextStyle(
              color: Theme.of(context).brightness == Brightness.light
                  ? Consts.APPBAR_LIGHT_COLOR
                  : Color(0xffFFFFFF)),
        ),
      ),
      body: BlocBuilder<SettingsCubit, SettingsState>(
          builder: (context, state) => state.when(
              neutral: (themeMode) => SettingsList(
                    darkBackgroundColor:
                        Theme.of(context).scaffoldBackgroundColor,
                    contentPadding: EdgeInsets.only(top: 20),
                    sections: [
                      SettingsSection(
                        title: 'Einstellungen',
                        tiles: [_themeTile(context, themeMode)],
                      ),
                      SettingsSection(
                        title: 'Informationen',
                        tiles: [
                          SettingsTile(
                            title: 'Datenquellen',
                            leading: Icon(Icons.info_outline_rounded),
                            onPressed: (c) => showDataSourceDialog(c),
                          ),
                          SettingsTile(
                            title: 'Open Source Lizenzen',
                            leading: Icon(Icons.notes),
                            onPressed: (c) =>
                                Navigator.push(c, OssLicensesPage.route()),
                          ),
                        ],
                      ),
                      SettingsSection(
                        title: 'Entwicklung',
                        tiles: [
                          SettingsTile(
                            title: 'Bewerte meine App',
                            subtitle: 'Vielen Dank :)',
                            leading: Icon(Icons.star_rate_rounded),
                            onPressed: (c) => launch(
                                "https://play.google.com/store/apps/details?id=org.uuuk.corona_daten"),
                          ),
                          SettingsTile(
                            title: 'Fehler melden',
                            leading: Icon(Icons.bug_report_rounded),
                            onPressed: (c) => showBugReportDialog(c),
                          ),
                          SettingsTile(
                            title: 'GitLab Repository',
                            leading: Icon(Icons.code),
                            onPressed: (c) => launch(Consts.URL_REPO),
                          ),
                        ],
                      )
                    ],
                  ))),
    );
  }
}
