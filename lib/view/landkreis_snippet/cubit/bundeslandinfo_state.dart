part of 'bundeslandinfo_cubit.dart';

@freezed
class BundeslandinfoState with _$BundeslandinfoState {
  const factory BundeslandinfoState.initial() = _Initial;
  const factory BundeslandinfoState.error() = _Error;
  const factory BundeslandinfoState.completed(Bundesland land) = _Completed;
}
