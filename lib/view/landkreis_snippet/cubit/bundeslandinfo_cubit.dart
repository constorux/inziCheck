import 'package:bloc/bloc.dart';
import 'package:corona_daten/services/corona_daten_api.dart';
import 'package:corona_daten/util/data_models.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'bundeslandinfo_state.dart';
part 'bundeslandinfo_cubit.freezed.dart';

class BundeslandinfoCubit extends Cubit<BundeslandinfoState> {
  final int bundeslandID;
  BundeslandinfoCubit(this.bundeslandID)
      : super(BundeslandinfoState.initial()) {
    init();
  }

  void init() {
    CoronaDatenApi.getBundeslandInfo(key: bundeslandID).then(
        (value) => emit(BundeslandinfoState.completed(value)),
        onError: (_) => emit(BundeslandinfoState.error()));
  }
}
