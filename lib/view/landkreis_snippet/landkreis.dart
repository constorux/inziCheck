import 'package:corona_daten/util/data_models.dart';
import 'package:corona_daten/view/landkreis_snippet/cubit/bundeslandinfo_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LandkreisWidget extends StatefulWidget {
  final Landkreis landkreis;
  final Function onMinus;
  final Color inzidenceColor;
  LandkreisWidget(this.landkreis,
      {required this.onMinus, required this.inzidenceColor})
      : super(key: Key(landkreis.name));

  @override
  _LandkreisWidgetState createState() => _LandkreisWidgetState();
}

class _LandkreisWidgetState extends State<LandkreisWidget>
    with SingleTickerProviderStateMixin {
  AnimationController? controller;
  Animation<Offset>? offset;
  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    offset = Tween<Offset>(begin: Offset.zero, end: Offset(1.0, 0.0))
        .animate(controller!);
  }

  Widget _valueField(bool colored, num? value,
      {String? title, int fracDigits = 0}) {
    return Expanded(
      //padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            (title ?? ""), //.toUpperCase(),
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.w600,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Text(
              (value != null) ? value.toStringAsFixed(fracDigits) : "--",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: colored
                      ? widget.inzidenceColor
                      : Theme.of(context).brightness == Brightness.dark
                          ? null
                          : Colors.grey[500],
                  fontWeight: FontWeight.bold,
                  fontSize: 36),
            ),
          ),
        ],
      ),
    );
  }

  Widget _blDataItem(BuildContext context,
      {required String name, required String value}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          Expanded(
            child: Text(
              name,
              style: TextStyle(
                  //fontSize: 15,
                  fontWeight: FontWeight.w600,
                  color: Colors.grey),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(value,
                style: TextStyle(
                  //fontSize: 15,
                  fontWeight: FontWeight.w600,
                )),
          ),
        ],
      ),
    );
  }

  Widget _mainDataSection(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            //Container(),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  widget.landkreis.name.toUpperCase(),
                  overflow: TextOverflow.fade,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            IconButton(
                onPressed: () {
                  print("onClick:" + widget.landkreis.name);
                  controller?.forward().whenCompleteOrCancel(() {
                    widget.onMinus();
                  });
                },
                splashRadius: 1,
                //enableFeedback: false,
                tooltip: "Landkreis entfernen",
                icon: Icon(Icons.remove))
          ]),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _valueField(true, widget.landkreis.inzidenz7Day,
                    fracDigits: 1, title: "7 Tage Inzidenz"),
                _valueField(false, widget.landkreis.casesAbsolute,
                    title: "Fälle letzte 7 Tage"),
              ],
            ),
          )
        ]);
  }

  Widget build(BuildContext context) {
    return BlocProvider(
        create: (c) => BundeslandinfoCubit(widget.landkreis.bundeslandID),
        child: SlideTransition(
            key: Key(widget.landkreis.name),
            position: offset!,
            child: Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: Container(
                  color: Theme.of(context).cardColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: _mainDataSection(context)),
                      BlocBuilder<BundeslandinfoCubit, BundeslandinfoState>(
                          builder: (context, state) => state.when(
                              initial: () => Container(),
                              error: () => Container(),
                              completed: (bundesland) => Container(
                                  color: Theme.of(context).canvasColor,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  child: Column(children: [
                                    _blDataItem(context,
                                        name: "Inzidenz in ${bundesland.name}",
                                        value: bundesland.inzidenz
                                            .toStringAsFixed(1)),
                                    _blDataItem(context,
                                        name:
                                            "Hosp. Rate in ${bundesland.name}",
                                        value: bundesland.hospitalisierungsrate
                                            .toStringAsFixed(1))
                                  ]))))
                    ],
                  ),
                ),
              ),
            )));
  }
}
