import 'package:corona_daten/cubits/landkreis_daten/landkreis_daten_cubit.dart';
import 'package:corona_daten/cubits/landkreis_list/landkreis_list_cubit.dart';
import 'package:corona_daten/util/consts.dart';
import 'package:corona_daten/util/data_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:select_dialog/select_dialog.dart';

import '../landkreis_snippet/landkreis.dart';

class HomePage extends StatelessWidget {
  void onAddClick(BuildContext homeContext, List<Landkreis> landkreise) async {
    return showDialog(
        context: homeContext,
        builder: (context) => BlocProvider<LandkreisListCubit>(
            create: (c) => LandkreisListCubit(),
            child: BlocBuilder<LandkreisListCubit, LandkreisListState>(
                builder: (context, state) => state.map(
                    loading: (_) => AlertDialog(
                        title: Text("Landkreis hinzufügen"),
                        content: ConstrainedBox(
                          constraints: BoxConstraints(maxHeight: 200),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )),
                    error: (_) => AlertDialog(
                        title: Text("Landkreis hinzufügen"),
                        content: ConstrainedBox(
                          constraints: BoxConstraints(maxHeight: 200),
                          child: Center(
                              child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(bottom: 10),
                                  child: IconButton(
                                      onPressed: () => context
                                          .read<LandkreisListCubit>()
                                          .getLandkreisNames(),
                                      icon: Icon(Icons.refresh_rounded))),
                              Text(
                                "Daten konnten nicht\ngeladen werden",
                                textAlign: TextAlign.center,
                              ),
                            ],
                          )),
                        )),
                    neutral: (state) => AlertDialog(
                        title: Text("Landkreis hinzufügen"),
                        content: SelectDialog(
                            searchBoxDecoration: InputDecoration(
                                fillColor: Theme.of(context).cardColor,
                                filled: true,
                                hintText: "Suche..."),
                            itemsList: state.counties,
                            onChange: (String selected) => homeContext
                                .read<LandkreisDatenCubit>()
                                .addItem(landkreise, selected)))))));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<LandkreisDatenCubit>(
              create: (c) => LandkreisDatenCubit()),
        ],
        child: Scaffold(
            appBar: AppBar(
              leading: BlocBuilder<LandkreisDatenCubit, LandkreisDatenState>(
                  builder: (context, state) => IconButton(
                      tooltip: "Landkreis hinzufügen",
                      onPressed: state.maybeWhen(
                          completed: (kreise) =>
                              () => onAddClick(context, kreise),
                          orElse: () => null),
                      icon: Icon(Icons.add))),
              centerTitle: true,
              title: Text(
                "InziCheck",
                style: TextStyle(
                    color: Theme.of(context).brightness == Brightness.light
                        ? Consts.APPBAR_LIGHT_COLOR
                        : Color(0xffFFFFFF)),
              ),
              actions: [
                IconButton(
                    onPressed: () => Navigator.pushNamed(context, "/dashboard"),
                    icon: Icon(Icons.query_stats)),
                IconButton(
                    onPressed: () => Navigator.pushNamed(context, "/settings"),
                    icon: Icon(Icons.settings_outlined)),
              ],
            ),
            body: BlocBuilder<LandkreisDatenCubit, LandkreisDatenState>(
                builder: (context, state) => state.map(
                    loading: (_) =>
                        Center(child: CircularProgressIndicator.adaptive()),
                    error: (errorState) => Center(
                            child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: IconButton(
                                    onPressed: () async {
                                      context
                                          .read<LandkreisDatenCubit>()
                                          .init();
                                    },
                                    icon: Icon(Icons.refresh_rounded))),
                            Text(
                              "Daten konnten nicht\ngeladen werden",
                              textAlign: TextAlign.center,
                            ),
                          ],
                        )),
                    completed: (completedState) => Padding(
                        padding: const EdgeInsets.all(12),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: RefreshIndicator(
                                onRefresh: () async {
                                  context.read<LandkreisDatenCubit>().init();
                                },
                                child: ListView(
                                  children: [
                                    if (completedState.counties.length == 0)
                                      Column(
                                        children: [
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(right: 8),
                                            child: Align(
                                              alignment: Alignment.centerLeft,
                                              child: ImageIcon(
                                                AssetImage(
                                                    "assets/icons/hint_arrow.png"),
                                                size: 50,
                                              ),
                                            ),
                                          ),
                                          Center(
                                            child: Text(
                                              "füge einen Landkreis hinzu",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                      fontStyle:
                                                          FontStyle.italic),
                                            ),
                                          )
                                        ],
                                      ),
                                    for (Landkreis lk
                                        in completedState.counties)
                                      LandkreisWidget(
                                        lk,
                                        inzidenceColor: LandkreisDatenCubit
                                            .getInzidenzColor(
                                                completedState.counties,
                                                lk.inzidenz7Day),
                                        onMinus: () => context
                                            .read<LandkreisDatenCubit>()
                                            .removeItem(completedState.counties,
                                                lk.name),
                                      )
                                  ],
                                ))))))));
  }
}
