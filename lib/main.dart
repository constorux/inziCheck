import 'package:corona_daten/cubits/settings/settings_cubit.dart';
import 'package:corona_daten/routes.dart';
import 'package:corona_daten/util/consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SettingsCubit>(
        create: (context) => SettingsCubit(),
        child: BlocBuilder<SettingsCubit, SettingsState>(
            builder: (context, state) => state.when(
                neutral: (themeMode) => MaterialApp(
                      debugShowCheckedModeBanner: false,
                      title: '7 Tage Inzidenz',
                      themeMode: themeMode,
                      theme: ThemeData(
                          brightness: Brightness.light,
                          scaffoldBackgroundColor: Color(0xFFEEEEEE),
                          cardColor: Color(0xFFFFFFFF),
                          appBarTheme: AppBarTheme(
                              brightness: Brightness.light,
                              backgroundColor: Color(0xFFEEEEEE),
                              iconTheme: IconThemeData(
                                  color: Consts.APPBAR_LIGHT_COLOR),
                              elevation: 0,
                              centerTitle: true,
                              shadowColor: Colors.transparent)),
                      darkTheme: ThemeData(
                          brightness: Brightness.dark,
                          scaffoldBackgroundColor: Color(0xFF222222),
                          cardColor: Color(0xFF343434),
                          appBarTheme: AppBarTheme(
                              brightness: Brightness.dark,
                              backgroundColor: Color(0xFF222222),
                              elevation: 0,
                              centerTitle: true,
                              shadowColor: Colors.transparent)
                          /* dark theme settings */
                          ),
                      routes: routes,
                      initialRoute: "/",
                    ))));
  }
}
