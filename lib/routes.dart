import 'package:corona_daten/view/home_page/home_page.dart';
import 'package:corona_daten/view/settings_page/settings_page.dart';
import 'package:corona_daten/view/web_dashboard/web_dashboard.dart';
import 'package:flutter/material.dart';

Map<String, Widget Function(BuildContext)> routes = {
  '/': (context) => HomePage(),
  '/settings': (context) => SettingsPage(),
  '/dashboard': (context) => WebDashboard(),
};
