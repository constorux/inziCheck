import 'dart:convert';

import 'package:corona_daten/util/data_models.dart';
import 'package:http/http.dart' as http;

const URL_BUNDESLAENDER = "https://api.corona-zahlen.org/states";

class CoronaDatenApi {
  static List<Bundesland> _bundeslaender = [];

  static Future<Bundesland> getBundeslandInfo({required int key}) async {
    for (var land in _bundeslaender) {
      if (land.id == key) return land;
    }

    String result = await http.read(Uri.parse(URL_BUNDESLAENDER));
    Map<String, dynamic> json = jsonDecode(result)["data"];
    List<Bundesland> neueBundeslaender = [];

    json.forEach((key, value) => neueBundeslaender.add(Bundesland(
        id: value["id"],
        name: value["name"],
        inzidenz: value["weekIncidence"],
        abkuerzung: key,
        hospitalisierungsrate: value["hospitalization"]["incidence7Days"])));

    _bundeslaender = neueBundeslaender;

    for (var land in _bundeslaender) {
      if (land.id == key) return land;
    }
    throw Exception("could not load Bundesland");
  }
}
