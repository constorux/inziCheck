import 'package:corona_daten/util/data_models.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

const URL_RKI_DASHBOARD =
    "https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/" +
        "Situationsberichte/COVID-19-Trends/COVID-19-Trends.html?__blob" +
        "=publicationFile#/home";

const URL_LANDKREISLIST =
    "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/" +
        "RKI_Landkreisdaten/FeatureServer/0/query" +
        "?where=1%3D1&outFields=county&returnGeometry=false&outSR=4326&f=json";

class RKIApi {
  static String _getCountyURL(
      {String field = "county", required String lkname}) {
    return "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/" +
        "services/RKI_Landkreisdaten/FeatureServer/0/query?where=" +
        field +
        "%20%3D%20'" +
        lkname +
        "'&outFields=cases,cases7_bl_per_100k,cases7_bl,death7_bl,cases7_lk," +
        "death7_lk,cases7_per_100k_txt,cases7_per_100k,EWZ_BL,cases_per_100k," +
        "county,cases_per_population,BL_ID" +
        "&returnGeometry=false&outSR=4326&f=json";
  }

  static Future<Landkreis> getLandkreis({required String name}) async {
    String result = await http.read(Uri.parse(_getCountyURL(lkname: name)));
    var json = jsonDecode(result);
    var features = json['features'];

    // use this to not break legacy support
    if (features.length == 0) {
      result =
          await http.read(Uri.parse(_getCountyURL(field: "GEN", lkname: name)));
      json = jsonDecode(result);
      features = json['features'];
    }
    if (features.length > 0) {
      var attributes = features[0]['attributes'];
      return Landkreis(
          name: name,
          bundeslandID: int.parse(attributes["BL_ID"]),
          inzidenz7Day: attributes["cases7_per_100k"],
          casesAbsolute: attributes["cases7_lk"],
          tode: attributes["deaths"]);
    }

    throw Exception("could not load Landkreis");
    // setsta
  }

  static Future<List<String>> getLandkreisList() async {
    List<String> results = [];

    try {
      String result = await http.read(Uri.parse(URL_LANDKREISLIST));
      var json = jsonDecode(result)['features'];
      for (int i = 0; i < json.length; i++) {
        results.add(json[i]['attributes']["county"]);
      }
    } catch (e) {
      throw Exception("error loading list");
    }

    return results;
  }
}
