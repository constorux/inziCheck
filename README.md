# InziCheck App

An application to see the Covid-19 '7 Tage Inzidenz' in german districts

## Getting Started

This project was built using Flutter. To build it yourself:
1. make sure you have `flutter` and `dart` installed
2. clone this repository 
3. run it by `flutter run` in the directory